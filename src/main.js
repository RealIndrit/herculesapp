// Modules to control application life and create native browser window
const path = require('path')
const { app } = require("electron");
const { ipcMain } = require("electron-better-ipc")

const AssetHelper = require("./helper/asset_helper")
const { WindowInstance } = require("./helper/window_helper.js");

let window = null;

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.

const gotTheLock = app.requestSingleInstanceLock()

if (!gotTheLock) {
    app.quit()
} else {
    app.on('second-instance', (event, commandLine, workingDirectory) => {
        // Someone tried to run a second instance, we should focus our window.
        if (window.window_instance) {
            if (window.window_instance.isMinimized()) window.window_instance.restore()
            window.window_instance.focus()
        }
    })

    // Create myWindow, load the rest of the app, etc...
    app.on('ready', () => {
        window = new WindowInstance(app)
            .setBrowserWindow({
                width: 800,
                height: 600,
                autoHideMenuBar: true,
                frame: false,
                icon: AssetHelper.BASE_URI() + 'assets/icons/256x256.ico',
                webPreferences: {
                    devTools: !app.isPackaged,
                    nodeIntegration: false,
                    contextIsolation: true,
                    preload: path.join(__dirname, './preload.js')
                }
            }, path.join(__dirname, 'index.html'))
            .createTrayContext(AssetHelper.BASE_URI() + 'assets/icons/256x256.ico', "Hercules")
            .updateTrayContext([{
                    label: 'Hide App',
                    click: function() {
                        window.window_instance.hide();
                    },
                },
                {
                    label: 'Quit',
                    click: function() {
                        app.quit();
                    },
                },
            ]).addTrayListener('click', () => {
                window.window_instance.show();
            }).addWindowListener('hide', function(event) {
                window.updateTrayContext([{
                        label: 'Show App',
                        click: function() {
                            window.window_instance.show();
                        },
                    },
                    {
                        label: 'Quit',
                        click: function() {
                            app.quit();
                        },
                    },
                ])
            }).addWindowListener('show', function(event) {
                window.updateTrayContext([{
                        label: 'Hide App',
                        click: function() {
                            window.window_instance.hide();
                        },
                    },
                    {
                        label: 'Quit',
                        click: function() {
                            app.quit();
                        },
                    },
                ])
            }).addWindowListener('minimize', function(event) {
                window.updateTrayContext([{
                        label: 'Show App',
                        click: function() {
                            window.window_instance.show();
                        },
                    },
                    {
                        label: 'Quit',
                        click: function() {
                            app.quit();
                        },
                    },
                ])
            }).addWindowListener('maximize', function(event) { // Notify Gui about window change
                ipcMain.callRenderer(window.window_instance, 'update/nav_buttons', true)
            }).addWindowListener('unmaximize', function(event) {
                ipcMain.callRenderer(window.window_instance, 'update/nav_buttons', false)
            });

        ipcMain.answerRenderer('app/close', () => {
            if (false) {
                window.window_instance.hide();
            } else {
                app.quit();
            }
        })

        ipcMain.answerRenderer('app/maximize_toggle', () => {
            window.window_instance.isMaximized() ? window.window_instance.unmaximize() : window.window_instance.maximize();

        })

        ipcMain.answerRenderer('app/minimize', () => {
            window.window_instance.minimize();
        })

        app.on('window-all-closed', function() {
            if (process.platform !== 'darwin') app.quit();
        })
    })
}