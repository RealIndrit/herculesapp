const axios = require('axios');


class APIHelper {
    constructor(cluster, database, collection, base_url, method, headers) {
        this.cluster = cluster;
        this.database = database;
        this.collection = collection;
        this.base_url = base_url;
        this.method = method;
        this.headers = headers;
    }

    sendRequest(data, endpoint) {
        return axios({
                method: this.method,
                url: (this.base_url + endpoint),
                headers: this.headers,
                data: data

            }).then(function(response) {
                return response.data
            })
            .catch(function(error) {
                return error;
            });
    };

    readMongoFor(filter, projection = {}, sort = {}, limit = 1000, skip = 0) {
        return this.sendRequest(
            JSON.stringify({
                dataSource: this.cluster,
                database: this.database,
                collection: this.collection,
                filter: filter,
                projection: projection,
                sort: sort,
                limit: limit,
                skip: skip
            }), "/action/find")
    }

    insertMongoFor(documents) {
        return this.sendRequest(
            JSON.stringify({
                dataSource: this.cluster,
                database: this.database,
                collection: this.collection,
                documents: documents
            }), "/action/insertMany")
    }

    replaceMongoFor(filter, replacement, upsert = false) {
        return this.sendRequest(
            JSON.stringify({
                dataSource: this.cluster,
                database: this.database,
                collection: this.collection,
                filter: filter,
                replacement: replacement,
                upsert: upsert
            }), "/action/replaceOne")
    }

    updateMongoFor(filter, update, upsert = false) {
        return this.sendRequest(JSON.stringify({
            dataSource: this.cluster,
            database: this.database,
            collection: this.collection,
            filter: filter,
            update: update,
            upsert: upsert
        }), "/action/updateMany")
    }

    deleteMongoFor(filter) {
        return this.sendRequest(
            JSON.stringify({
                dataSource: this.cluster,
                database: this.database,
                collection: this.collection,
                filter: filter
            }), "/action/deleteMany")
    }

    aggregateMongoFor(pipeline) {
        return this.sendRequest(
            JSON.stringify({
                dataSource: this.cluster,
                database: this.database,
                collection: this.collection,
                pipeline: pipeline
            }), "/action/aggregate")
    }
}


module.exports = {
    APIHelper
}