const { MongoClient, ServerApiVersion } = require('mongodb');
const AssetHelper = require("./asset_helper")
const path = require('path')
const credentials = path.join(AssetHelper.BASE_URI(), 'X509-cert.pem')

class MongoInstance {
    constructor(database_target) {
        this.client = new MongoClient('mongodb+srv://cluster0.z6koi.mongodb.net/hercules?authSource=%24external&authMechanism=MONGODB-X509&retryWrites=true&w=majority', {
            sslKey: credentials,
            sslCert: credentials,
            serverApi: ServerApiVersion.v1
        });

        this.database = this.client.db(database_target);
    }

    async createListingByData(collection_target, data) {
        try {
            // Connect to the MongoDB cluster
            await this.client.connect();
            const result = await this.database.collection(collection_target).insertOne(data);
            console.log(`New listing created with the following id: ${result.insertedId}`);
            this.client.close();
            // Make the appropriate DB calls

        } finally {
            // Close the connection to the MongoDB cluster
            await this.client.close();
        }

    }

    async findListingByData(collection_target, data) {
        try {
            await this.client.connect();
            const result = await this.database.collection(collection_target).find(data);

            if (result) {
                console.log(`Found a listing in the collection with the data '${data}':`);
                this.client.close();
                return result;
            } else {
                console.log(`No listings found with the data '${data}'`);
                this.client.close();
                return null;
            }
        } finally {
            // Close the connection to the MongoDB cluster
            await this.client.close();
        }
    }

    async updateListingByData(collection_target, data, updated_data, upsert_flag = false) {
        try {
            await this.client.connect();
            const result = await this.database.collection(collection_target)
                .updateOne(data, { $set: updated_data }, { upsert: upsert_flag });

            console.log(`${result.matchedCount} document(s) matched the query criteria.`);
            console.log(`${result.modifiedCount} document(s) was/were updated.`);
            this.client.close();
        } finally {
            // Close the connection to the MongoDB cluster
            await this.client.close();
        }
    }

    async upsertListingByData(collection_target, data, updated_data) {
        this.updateListingByData(collection_target, data, updated_data, true);
    }

    async deleteListingByData(collection_target, data) {
        try {
            await this.client.connect();
            const result = await this.database.collection(collection_target).deleteOne(data)
            console.log(`${result.deletedCount} document(s) was/were deleted.`);
            this.client.close();
        } finally {
            // Close the connection to the MongoDB cluster
            await this.client.close();
        }
    }
}

module.exports = {
    MongoInstance
}