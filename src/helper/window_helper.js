const { BrowserWindow, Tray, Menu } = require("electron");

class WindowInstance {
    constructor(instance) {
        this.app = instance;
    }

    setBrowserWindow(spec, source) {
        this.window_instance = new BrowserWindow(spec);
        this.window_instance.loadFile(source);
        return this;
    }

    updateTrayContext(spec) {
        this.tray_instance.setContextMenu(Menu.buildFromTemplate(spec));
        return this;
    }

    addWindowListener(event_type, action) {
        this.window_instance.addListener(event_type, action);
        return this;
    }

    removeWindowListener(event_type) {
        this.window_instance.removeListener(event_type, action);
        return this;
    }

    clearWindowListeners() {
        this.window_instance.removeAllListeners();
    }

    updateTrayContext(spec) {
        this.tray_instance.setContextMenu(Menu.buildFromTemplate(spec));
        return this;
    }

    addTrayListener(event_type, action) {
        this.tray_instance.addListener(event_type, action);
        return this;
    }

    removeTrayListener(event_type) {
        this.tray_instance.removeListener(event_type, action);
        return this;
    }

    clearTrayListeners() {
        this.tray_instance.removeAllListeners();
    }
    createTrayContext(guid, tooltip) {
        this.tray_instance = new Tray(guid);
        this.tray_instance.setToolTip(tooltip);
        return this;

    }
}

module.exports = {
    WindowInstance
}