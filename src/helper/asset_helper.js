const path = require('path')

module.exports = {
    BASE_URI: () => path.join(__dirname, '../').toString()
}