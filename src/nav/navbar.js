// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.

const MINMIZE_APP_BUTTON = document.getElementById("minimize-app")
const MAXIMIZE_APP_BUTTON = document.getElementById("maximize-app")
const UNMAXIMIZE_APP_BUTTON = document.getElementById("unmaximize-app")
const CLOSE_APP_BUTTON = document.getElementById("close-app")
const SHOW_MENU_BUTTON = document.getElementById("show-menu")

//Change to onclick
MINMIZE_APP_BUTTON.addEventListener("click", minimize_app)
MAXIMIZE_APP_BUTTON.addEventListener("click", maximize_toggle_app)
UNMAXIMIZE_APP_BUTTON.addEventListener("click", maximize_toggle_app)
CLOSE_APP_BUTTON.addEventListener("click", close_app)
SHOW_MENU_BUTTON.addEventListener("click", show_menu)

function close_app() {
    gui.window.close_app();
}

function minimize_app() {
    gui.window.minimize_app();
}

function maximize_toggle_app() {
    gui.window.maximize_toggle_app();
}

function update_nav_btns(maximize_bool) {
    var unmax = document.getElementById("unmaximize-app");
    var max = document.getElementById("maximize-app");
    if (maximize_bool) {
        max.style.display = "inline-block";
        unmax.style.display = "none";
    } else {
        unmax.style.display = "inline-block";
        max.style.display = "none";
    }
}

function show_menu() {
    console.log("show_menu_event")
}