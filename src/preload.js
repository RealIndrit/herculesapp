// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.

const { contextBridge } = require('electron')
const { ipcRenderer } = require('electron-better-ipc')

// Register IPC connection from main

const guiAPI = { //can only be accessed from public side js
    window: {
        close_app: () => ipcRenderer.callMain("app/close"),
        maximize_toggle_app: () => ipcRenderer.callMain("app/maximize_toggle"),
        minimize_app: () => ipcRenderer.callMain("app/minimize")
    }
}

ipcRenderer.answerMain("update/nav_buttons", (maximize_bool) => {
    // Somehow update client gui
    console.log(maximize_bool)
})

contextBridge.exposeInMainWorld('gui', guiAPI);